# SKC [![Netlify Status](https://api.netlify.com/api/v1/badges/7d40d9e7-9fe9-4922-a390-061afbd52340/deploy-status)](https://app.netlify.com/sites/clever-sammet-dcc5eb/deploys)

### Shingitai Karate Club


## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
